import axios from "axios";
import { useEffect, useState } from "react";
import { DropDown } from "../Components/DropDown";
import { FormOperation } from "../Components/FormOperation";
import { Operation } from "../Components/Operation";
import SearchBar from "../Components/Search";
import "./Home.css"

export function Home(){
    const[operationBudget, setOperation]= useState([]);


    async function displayOperations(){
        const response = await axios.get('https://budget-projet.herokuapp.com/api/budget')
        setOperation(response.data);
    }

    async  function deleteOperation(id){
        await axios.delete('https://budget-projet.herokuapp.com/api/budget/'+id);
        setOperation(
            operationBudget.filter(item => item.id !== id)
        );
    }

    async function addOperation(operation){
        const response = await axios.post('https://budget-projet.herokuapp.com/api/budget', operation);
        
        setOperation([
            ...operationBudget,
            response.data
        ]);

    }

    async function findByMonth(date){
        const response = await axios.get('https://budget-projet.herokuapp.com/api/budget/operations/month/'+ date);
        setOperation(response.data);

    }

     function Price(){
        let total = 0;
        for (const element of operationBudget) {
            total += element.montant


        }return total
    }
    


    useEffect(() =>{
        displayOperations();
    }, []);

    const {search} = window.location;
    const query = new URLSearchParams(search).get("s");
    const [searchQuery, setSearchQuery] = useState(query || "");

    const filterOperations = (operationBudget, query) =>{
        if(!query){
            return operationBudget;
        }

        return operationBudget.filter((operation) =>{
            const operationCategory = operation.categorie;
            return operationCategory.includes(query);
        });
    };

    const filteredOperations = filterOperations(operationBudget, query);


    return(
        <div className="container conteneur">
            <h1>Mes opérations</h1>
            
            <p className="total">Total : {Price()} €
            
            </p>
            
            
            <div className="row">
            <div className="col-8 ">
            {filteredOperations.map(item =>
             <Operation key={item.id}
            operation={item}
            onDelete={deleteOperation}
             /> )}
            </div>

            <div className="col-4">
            <p><DropDown reset={displayOperations} selectMonth={findByMonth} /></p>
            <SearchBar
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            />
            
                <h4 className="mt-5">Ajouter une opération</h4>
            <FormOperation onFormSubmit={addOperation}/>
            </div>
            </div>
           
            

        </div>
    )

}