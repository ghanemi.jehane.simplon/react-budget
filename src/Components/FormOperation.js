import { useState } from "react"
import { Form, Button } from "react-bootstrap";

const initialState = {
    montant: '',
    categorie: '',
    date: ''
}

export function FormOperation({onFormSubmit}){
    const [form, setForm] = useState(initialState);

    function handleChange(event){
        setForm({
            ...form,
            [event.target.name]:event.target.value
        });
    }


function handleSubmit(event){
    event.preventDefault();
    onFormSubmit(form);
}


return(
    
    <Form onSubmit={handleSubmit}>

      <Form.Group className="mb-3" controlId="formBasicEmail">
    
         <Form.Label>Montant</Form.Label>
        <Form.Control type="text" name='montant' onChange={handleChange} value= {form.montant} />
    
         <Form.Label>Catégorie</Form.Label>
        <Form.Control type="text" name="categorie" onChange={handleChange} value= {form.categorie} />
    
        <Form.Label>Date</Form.Label>
        <Form.Control type="date" name = "date" onChange={handleChange} value= {form.date} />
    
       </Form.Group>
    
     
       <Button variant="info" type="submit">
         Submit
       </Button>
    </Form>


    

  
)

}




  // <form onSubmit={handleSubmit}>
        
    //     <label>Montant </label>
    //     <input type="text" name="montant" onChange={handleChange} value= {form.montant}/>

    //     <label>Catégorie: </label>
    //     <input type="text" name="categorie" onChange={handleChange} value= {form.categorie}/>

    //     <label>Date </label>
    //     <input type="date" name="date" onChange={handleChange} value= {form.date}/>

    //     <button>Submit</button>
    // </form>