# Budget APP Project

Projet réalisé en solo avec une seule entity Operation sans les options

## Technologies utilisées
- React 
- Javascript
- Node JS
- Express
- MySQL
- Super Test


## Fonctionnalités

- Consulter les opérations
- Ajouter une opération 
- Supprimer une opération
- Rechercher une opération par catégorie
- Rechercher une opération par mois (n'apparaît pas dans le front)
- Voir le total


## Maquettes : 
![wireframe Budget](src/Images/maquette1.png)
![wireframe Budget](src/Images/maquette2.png)

## Lien Heroku Back-end:
(https://budget-projet.herokuapp.com/api/budget)

## Liens Front-end: 
- (https://hardcore-villani-8e4746.netlify.app/)
- (https://react-budget-projet.herokuapp.com/)
