import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from './Pages/Home';
import { Nav } from './Components/Nav';

function App() {
  return (
   <div>
     <BrowserRouter>
     <div>
       <Nav/>
       <Switch>
       <Route exact path='/'>
         <Home/>
         </Route>
         </Switch>
     </div>
     </BrowserRouter>
     
   </div>
  );
}

export default App;
