import { Button, Form } from "react-bootstrap";

export default function SearchBar({searchQuery, setSearchQuery}){

    return(
        <Form>
  <Form.Group action="/" methode="get" autoComplete="off" className="mb-3" controlId="exampleForm.ControlInput1">
    <Form.Label>Rechercher une opération</Form.Label>
    <Form.Control value={searchQuery} onInput={e => setSearchQuery (e.target.value)}type="text" placeholder="Opération" name="s" 
     />
      
  </Form.Group>
  <Button variant="info" type="submit">
         Submit
       </Button>
</Form>

        )
}