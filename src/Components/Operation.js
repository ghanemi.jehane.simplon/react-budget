import {Table } from "react-bootstrap";

export function Operation({operation, onDelete}){
  
    return(
       <div>
         <Table striped bordered hover variant="primary">
  <thead>
    <tr>
      <th>#</th>
      <th>Catégorie</th>
      <th>Date</th>
      <th>Montant</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{operation.id}</td>
      <td>{operation.categorie}</td>
      <td>{operation.date}</td>
      <td>{operation.montant} € </td>
    </tr>
  </tbody>
</Table>
    </div>    
                
                
            
     
    )
  
  }